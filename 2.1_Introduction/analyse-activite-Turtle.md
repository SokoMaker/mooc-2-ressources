
<!--- 
  Auteur : François MARQUES
  Date : 2023-02-04

  FUN MOOC Apprendre à enseigner le Numérique et les Sciences Informatiques

  Analyse de l'activité Turtle de (Maxime Fourny) récupérée sur internet
-->

# 2.1 Introduction - Analyse de l'activité Turtle

## Objectifs

## Pré-requis à cette activité

## Durée de l'activité

## Exercices cibles

## Description du déroulement de l'activité

## Anticipation des difficultés des élèves

## Gestion de l'hétérogénéïté
